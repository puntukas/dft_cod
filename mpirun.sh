#!/bin/bash

WORKDIR="/home/vytautas/qe_cod/organics"
CIF_DIR="$WORKDIR/CIFs/" # Directory where CIF files are located
CIF_FILES="$CIF_DIR*"
PSEUDO_DIR="/home/vytautas/qe_cod/qe_psps/PBE/" # Path to directory with PBE pseudopotentials
CALC_TYPE="vc-relax"  # scf, relax or vc-relax

for CIF_FILE in $CIF_FILES
do
    PREFIX=$CIF_DIR
    QE_PREFIX=${CIF_FILE#$PREFIX}
    QE_PREFIX=${QE_PREFIX%".cif"}

    echo "$QE_PREFIX.pwi"
    sed -i "s@outdir.*@outdir = '$WORKDIR/$QE_PREFIX'@" $WORKDIR/$QE_PREFIX.pwi
    sed -i "s@pseudo_dir.*@pseudo_dir = '$PSEUDO_DIR'@" $WORKDIR/$QE_PREFIX.pwi	
    mpirun -np 36 pw.x < "$WORKDIR/$QE_PREFIX.pwi" > "$WORKDIR/$QE_PREFIX.pwo" &        
    wait
    mv $WORKDIR/$QE_PREFIX.pwi $WORKDIR/$QE_PREFIX/
    mv $WORKDIR/$QE_PREFIX.pwo $WORKDIR/$QE_PREFIX/
    cp $CIF_FILE $WORKDIR/$QE_PREFIX/
done
